    const log = Vue.createApp({})

    log.component('compteur', {
        data() {
            return {
                comptage : 0
            }
        },
        template: '<button v-on:click="comptage++"> Vous m\'avez cliqué {{ comptage }} fois. </button>'
    })

    log.mount('#ancre');