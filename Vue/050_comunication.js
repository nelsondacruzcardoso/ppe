const log =Vue.createApp({
    data() {
        return {
            listeSLAM: [
                {
                    id: 1,
                    code: 'SLAM3',
                    libelle: 'Data'
                },
                {
                    id: 2,
                    code: 'SLAM4',
                    libelle: 'Dev'
                },
                {
                    id: 3,
                    code: 'PPE',
                    libelle: 'Projet'
                }
            ]
        }
    }
})

log.component('afaire-item', {
    props: {
        liste: Object
    },
    template: '<li> {{ liste.code }} - {{ liste.libelle }} </li>'
})

log.mount('#slam')