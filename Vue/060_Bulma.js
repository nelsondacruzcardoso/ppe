const log =Vue.createApp({
    data() {
        return {

        }
    }
});

log.component('competence', {
    props: {
        titre: String,
        contenu: String
    },

    data() {
        return {

        }
    },

    template: `
    <article class="message">
        <div class="message-header">
        {{ titre }}
        <span class="close">x</span>
        </div>
        <div class="message-body">
        {{ contenu }}
        </div>
    </article>
    `
})

log.mount('#slam')